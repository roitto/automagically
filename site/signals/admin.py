from signals.models import Transform, DeviceCommand, StoreGlobalVariable, FindRepeat
from django.forms import ModelForm
from django.contrib import admin

class TransformAdmin(admin.ModelAdmin):
    exclude = ('regexp',)

class DeviceCommandAdmin(admin.ModelAdmin):
    exclude = ('regexp',)

class StoreGlobalVariableAdmin(admin.ModelAdmin):
    exclude = ('regexp',)

class FindRepeatAdmin(admin.ModelAdmin):
    exclude = ('regexp',)


admin.site.register(Transform, TransformAdmin)
admin.site.register(DeviceCommand, DeviceCommandAdmin)
admin.site.register(FindRepeat, FindRepeatAdmin)
admin.site.register(StoreGlobalVariable, StoreGlobalVariableAdmin)

