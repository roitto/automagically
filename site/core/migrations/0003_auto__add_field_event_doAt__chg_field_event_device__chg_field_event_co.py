# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Event.doAt'
        db.add_column('core_event', 'doAt',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 5, 24, 0, 0)),
                      keep_default=False)


        # Renaming column for 'Event.device' to match new field type.
        db.rename_column('core_event', 'device', 'device_id')
        # Changing field 'Event.device'
        db.alter_column('core_event', 'device_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device']))
        # Adding index on 'Event', fields ['device']
        db.create_index('core_event', ['device_id'])


        # Renaming column for 'Event.command' to match new field type.
        db.rename_column('core_event', 'command', 'command_id')
        # Changing field 'Event.command'
        db.alter_column('core_event', 'command_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Command']))
        # Adding index on 'Event', fields ['command']
        db.create_index('core_event', ['command_id'])

    def backwards(self, orm):
        # Removing index on 'Event', fields ['command']
        db.delete_index('core_event', ['command_id'])

        # Removing index on 'Event', fields ['device']
        db.delete_index('core_event', ['device_id'])

        # Deleting field 'Event.doAt'
        db.delete_column('core_event', 'doAt')


        # Renaming column for 'Event.device' to match new field type.
        db.rename_column('core_event', 'device_id', 'device')
        # Changing field 'Event.device'
        db.alter_column('core_event', 'device', self.gf('django.db.models.fields.IntegerField')())

        # Renaming column for 'Event.command' to match new field type.
        db.rename_column('core_event', 'command_id', 'command')
        # Changing field 'Event.command'
        db.alter_column('core_event', 'command', self.gf('django.db.models.fields.CharField')(max_length=20))
    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'htmlText': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'core.event': {
            'Meta': {'object_name': 'Event'},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'doAt': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'core.groupdevice': {
            'Meta': {'object_name': 'GroupDevice', '_ormbases': ['core.Device']},
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevices': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'subdevices'", 'symmetrical': 'False', 'to': "orm['core.Device']"})
        },
        'core.rawdevice': {
            'Meta': {'object_name': 'RawDevice', '_ormbases': ['core.Device']},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'deviceId': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'}),
            'protocol': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'protocolModel': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20'}),
            'rawName': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '20', 'blank': 'True'})
        },
        'core.timerdevice': {
            'Meta': {'object_name': 'TimerDevice', '_ormbases': ['core.Device']},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'default': '102', 'to': "orm['core.Command']"}),
            'delayMinutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1'}),
            'device_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['core.Device']", 'unique': 'True', 'primary_key': 'True'}),
            'subDevice': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'subdevice'", 'to': "orm['core.Device']"})
        }
    }

    complete_apps = ['core']