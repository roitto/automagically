# Imports
from django.db import models
from settings.models import getSettings
from general import getSetting
import signals.models
import Queue
import urllib2, json
from time import strftime, localtime
from datetime import datetime, timedelta

# Plugin configurations
PLUGIN_NAME = 'smhi'
PLUGIN_ENABLED =  'Enabled'
PLUGIN_INTERVAL = 'Interval'
PLUGIN_DATAPOINTS = 'Datapoints'


# Global variables
debug = False
workQueue = Queue.Queue()

def smhi_parse(lat, lon, datapoints):
    url='http://opendata-download-metfcst.smhi.se/api/category/pmp1g/version/1/geopoint/lat/'+str(lat)+'/lon/'+str(lon)+'/data.json'
    if debug:
        print url  
    try:
        j = urllib2.urlopen(url)
        j_obj = json.load(j)
    except:
        print 'While fetching data from SMHI with' + PLUGIN_NAME + ', the plugin threw an exception'
    
    output = PLUGIN_NAME + ',temperature'
    for predition_time in j_obj['timeseries']:
        for i in range(0, int(float(datapoints))):
            if predition_time['validTime'] == (datetime.now() + timedelta(hours=i)).strftime("%Y-%m-%dT%H:00:00Z"):
                output += ","+ str(predition_time['t'])

    signals.models.postToQueue(output, PLUGIN_NAME)


# Signal handler
def signalHandler(signal):
    if debug:
        print PLUGIN_NAME + ' received signal:' + signal.content.strip()
    if signal.content == 'terminate':
        workQueue.put(None)
    elif signal.content.strip() == 'general,configuration,changed':
        workQueue.put('generalupdate')
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.startswith(PLUGIN_NAME + ',action:'): # Note: Change to your needs
        # Received an 'action' signal
        for i in signal.content.strip()[12:].split(';'):
            if i != '':
                workQueue.put(i)

# Thread function
def threadFunc():
    currentSettings = {}
    keepRunning = True

    # Load plugin settings
    currentSettings = getSettings(PLUGIN_NAME)
    pluginEnabled = currentSettings[PLUGIN_ENABLED]
    pluginDatapoints = currentSettings[PLUGIN_DATAPOINTS]
    nextUpdate = currentSettings[PLUGIN_INTERVAL]

    # Load settings
    lat=str(getSetting('Location Latitude'))
    lon=str(getSetting('Location Longitude'))
    
    # Main loop
    while(keepRunning):
        try:
            try:
                if pluginEnabled and nextUpdate > 0:
                    s = workQueue.get(True, nextUpdate) # Wait for a signal or a timeout
                else:
                    s = workQueue.get(True) # Wait for a signal
                
            except:
                # Exception occurs when nextUpdate time has elapsed
                s = 'TIMEOUT'
                if debug:
                    print PLUGIN_NAME + ' time to act'

            if s == None: # Time to close down
                if debug:
                    print PLUGIN_NAME + ' time to close down'
                workQueue.task_done()
                keepRunning = False
                continue
                
            elif s == 'generalupdate': # Configuration has changed, update to new values
                if debug:
                    print 'General settings updated, affecting ' + PLUGIN_NAME
                lat=str(getSetting('Location Latitude'))
                lon=str(getSetting('Location Longitude'))
                workQueue.task_done()
                continue

            elif s == 'update': # Configuration has changed, update to new values
                if debug:
                    print PLUGIN_NAME + ' settings updated'
                currentSettings = getSettings(PLUGIN_NAME)
                pluginEnabled = currentSettings[PLUGIN_ENABLED]
                pluginDatapoints = currentSettings[PLUGIN_DATAPOINTS]
                nextUpdate = currentSettings[PLUGIN_INTERVAL]
                
                ##
                ## CODE HERE TO ACT WHEN CONFIGURATION CHANGES OCCURS
                ##
                
                workQueue.task_done()
                continue
    
            elif s == 'TIMEOUT':
                if debug:
                    print PLUGIN_NAME + ' acting on timeout'
                if pluginEnabled:
                    smhi_parse(lat, lon, pluginDatapoints)
                
                # Note: This part should NOT have a workQueue.task_done()
                continue
                
            else: # A signal was received
                if debug:
                    print PLUGIN_NAME + ' got signal:' + s
                if pluginEnabled:
                    # Expecting: "fetch,N" where N is number of datapoints to fetch, e.g "smhi,action:fetch,4"
                    _s=s.split(',')
                    if len(_s) == 2:
                        if _s[0] == 'fetch':
                            smhi_parse(lat, lon, _s[1])

                workQueue.task_done()
                continue

        except:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            if debug:
                raise

def init():
    settings = {PLUGIN_ENABLED:     ('boolean', False),
                PLUGIN_DATAPOINTS:  ('integer', 3),
                PLUGIN_INTERVAL:    ('integer', 60)}
    return ([PLUGIN_NAME, 'general'], settings, signalHandler, threadFunc)
