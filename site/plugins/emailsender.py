from django.db import models
import signals.models
import Queue
import smtplib
import email.mime.text
from settings.models import getSettings

PLUGIN_NAME = 'emailsender'

debug = False

workQueue = Queue.Queue()

global currentSettings
currentSettings = {}

def signalHandler(signal):
    global currentSettings
    if not currentSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        currentSettings = getSettings(PLUGIN_NAME)
        print 'configuration updated'
        print currentSettings

    if signal.content == 'terminate':
        workQueue.put(None)        

    elif signal.content.startswith('email:'):        
        workQueue.put(signal.content[6:].strip())

def threadFunc():
    keepRunning = True
    while(keepRunning):
        try:
            s = workQueue.get()

            if s == None:
                print 'Got a none from email workQueue'
                workQueue.task_done()
                keepRunning = False
                continue

            parts = s.split(':', 2)

            _from = currentSettings['From']

            if len(parts) == 3:
                _to = parts[0]
                if _to == '':
                    _to = currentSettings['Default to']
                _subject = parts[1]
                _msg = parts[2]
            elif len(parts) == 2:
                _to = currentSettings['Default to']
                if _to == '':
                    raise ValueError('No Default to set when sending email')

                _subject = parts[0]
                _msg = parts[1]
            else:
                print parts
                raise ValueError('Not well formed email signal should be email:toaddress:subject:message text')
            
            if debug:
                print 'Sending email'
                print 'From:', _from
                print 'To:', _to
                print 'Subject:', _subject
                print 'Message text:', _msg
            
            msg = email.mime.text.MIMEText(_msg)

            msg['Subject'] = _subject
            msg['From'] = _from
            msg['To'] = _to

            s = smtplib.SMTP(currentSettings['SMTP-server'], currentSettings['SMTP-port'])
            if(currentSettings['SMTP-user'] != ''):
                s.starttls()  
                s.login(currentSettings['SMTP-user'],currentSettings['SMTP-password'])

            s.sendmail(currentSettings['From'], [_to], msg.as_string())
            s.quit()

            workQueue.task_done()

        except:
            print 'Error executing email send command'
            if debug:
                raise

def init():
    settings = {'SMTP-server': ('string', 'mail.google.com'),
                'SMTP-port': ('integer', 557),
                'SMTP-user': ('string', ''),
                'SMTP-password': ('string', ''),
                'From': ('string', ''),
                'Default to': ('string', '')}

    return ('email', settings, signalHandler, threadFunc)
