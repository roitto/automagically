from django.db import models
import signals.models
import Queue
import urllib
import urllib2

PLUGIN_NAME = 'httpreq'

debug = False

workQueue = Queue.Queue()

def signalHandler(signal):
    if signal.content == 'terminate':
        workQueue.put(None)        

    elif signal.content.startswith('httpreq,do:'):        
        workQueue.put(signal.content[11:].strip())

def threadFunc():
    keepRunning = True
    while(keepRunning):
        try:
            s = workQueue.get()

            if s == None:
                print 'Got a none from httpreq workQueue'
                workQueue.task_done()
                keepRunning = False
                continue

            if not (s.startswith('http://') or s.startswith('https://')):
                s = 'http://' + s

            parts = s.split(':')
            url = parts[0]+':'+parts[1]

            headersDict = {}
            data = None
            print parts
            if len(parts) == 4:
                for h in parts[2].split(';'):
                    try:
                        _t = h.split('=')
                        headersDict[_t[0]] = _t[1]
                    except:
                        print 'Error building headerDict'
                        raise

                dataDict = {}
                for d in parts[3].split(';'):
                    try:
                        _t = d.split('=')
                        dataDict[_t[0]] = _t[1]
                    except:
                        print 'Error building dataDict'
                        raise


                if dataDict:
                    data = urllib.urlencode(dataDict)


            if debug:
                print 'HTTP REQ plugin:\nURL:', url
                print 'headers:', headersDict
                print 'data:', data

            req = urllib2.Request(url, data = data, headers = headersDict)

            u = urllib2.urlopen(req)
            if debug:
                print u.read()                

            workQueue.task_done()

        except:
            print 'Error executing http request command'
            if debug:
                raise

def init():
    settings = {}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
