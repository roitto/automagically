#!/bin/bash
cd /home/pi/source/automagically
git pull
cd /home/pi/source/automagically/site
sudo /etc/init.d/apache2 stop
sudo /etc/init.d/automagically stop
sudo /etc/init.d/mysql restart
python manage.py migrate core
python manage.py migrate signals
python manage.py migrate settings
python manage.py migrate remote
mysql --user root --password=raspberry automagically < db_change_storage_engine.sql
python manage.py collectstatic --noinput
echo " " > plugins/signaldebug.txt
chmod 666 plugins/signaldebug.txt


